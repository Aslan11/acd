package midterm;

public class Task4 {
    public static int Buy(int arr[], int n) {
        int x = arr[0];

        for (int i = 1; i < n; i++)
            x = Math.min(x, arr[i]);
        return x;
    }

    public static int Sell(int arr[], int n) {
        int sell = arr[0];

        for (int i = 1; i < n; i++)
            sell = Math.max(sell, arr[i]);
        return sell;
    }

    public static void main(String[] args) {

        int arr[] = { 10, 7, 5, 8, 11 };
        int n = arr.length;

    }

}
