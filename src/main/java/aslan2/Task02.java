package aslan2;

import java.util.Scanner;

public class Task02 {
    public void counter(int x){
        if(x==0) return ;

        Scanner y = new Scanner(System.in);
        int b=y.nextInt();

        x=x-1;
        counter(x);
        System.out.println(b);

    }

    public void run() {
        Scanner x = new Scanner(System.in);
        int c = x.nextInt();
        counter(c);
    }
}
