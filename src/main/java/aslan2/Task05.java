package aslan2;

import java.util.Scanner;

public class Task05 {
    static void rec(int arr[], int n, int k, int index) {
        int i;
        if (k == 0) {
            for (i = 0; i < index; i++)
                System.out.print(arr[i] + " ");
            System.out.println();
        }
        if (k > 0) {
            for (i = 1; i <= n; ++i) {
                arr[index] = i;
                rec(arr, n, k - 1, index + 1);
            }
        }
    }
    public void run()
    {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int k = scan.nextInt();
        int arr[] = new int[k];
        rec(arr, n, k, 0);
    }
}
