package aslan2;

import java.util.Scanner;

public class Task03 {
    public void counter(int c){
        if(c==0) return ;

        Scanner s = new Scanner(System.in);
        String b=s.nextLine();

        c=c-1;

        counter(c);
        System.out.println(b);

    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int c = s.nextInt();
        counter(c);
    }
}
