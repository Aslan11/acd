package aslan2;

import java.util.Scanner;

public class Task04 {
    public void run() {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int arr[][] = new int[n][n];
        rec(1, n, 0, 0,arr);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
    }
    public void rec(int sum, int a, int y, int x,int arr[][]) {
        if (sum <= a * a) {
            for (int j = y; j <= a - 1 - y; j++) {
                if (sum <= a * a) {
                    arr[x][j] = sum;
                    sum++;
                }
            }
            for (int i = x; i <= a - 1 - x; i++) {
                if (i == x) continue;
                if (sum <= a * a) {
                    arr[i][a - y - 1] = sum;
                    sum++;
                }
            }
            for (int j = a - y - 1; j >= y; j--) {
                if (j == a - y - 1) continue;
                if (sum <= a * a) {
                    arr[a - x - 1][j] = sum;
                    sum++;
                }
            }
            for (int i = a - x - 1; i >= x; i--) {
                if (i == a - x - 1) continue;
                if (i == x) continue;
                if (sum <= a * a) {
                    arr[i][y] = sum;
                    sum++;
                }
            }
            rec(sum, a, y+1, x+1,arr);
        }
    }
}
