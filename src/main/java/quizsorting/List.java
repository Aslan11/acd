package quizsorting;

public class List {
    private Student[] studentList;
    private int size = 0;

    public List() {
        studentList = new Student[150];
    }

    public void add(Student student) {
        for (int i = 0; i < size; i++) {
            studentList[i] = student;
        }
        size++;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(studentList[i]);
        }
    }


    public void sort(String type, String order) {
        if(type.equals("name_a") && order.equals("asc")) {
            // sort_name_a_asc();
        } else if(type.equals("name_a") && order.equals("desc")) {
            // sort_name_a_asc();
            // sort_name_a_desc();
        } else if(type.equals("name_l") && order.equals("asc")) {
            // sort_name_a_asc();
            // sort_name_l_asc();
        } else if(type.equals("name_l") && order.equals("desc")) {
            //sort_name_a_asc();
            // sort_name_l_desc();
        } else if(type.equals("age") && order.equals("asc")) {
            // sort_name_a_asc();
            sort_age_asc();
        } else if(type.equals("age") && order.equals("desc")) {
            //  sort_name_a_asc();
            //  sort_age_desc();
        } else if(type.equals("gpa") && order.equals("asc")) {
            // sort_name_a_asc();
            //sort_gpa_asc();
        } else if(type.equals("gpa") && order.equals("desc")) {
            //sort_name_a_asc();
            //sort_gpa_desc();
        }
    }

        public void sort_name_a_asc(){
            String temp;
            List studentList = new List();
            String names[] = new String[50];
            for (int i = 0; i < 50; i++) {
                for (int j = i + 1; j < 50; j++) {
                    if (names[i].compareTo(names[j])>0) {
                        temp = names[i];
                        names[i] = names[j];
                        names[j] = temp;
                    }
                }
            }
            System.out.print("Names in Sorted Order:");
            for (int i = 0; i < 50 - 1; i++) {
                System.out.print(names[i] + ",");
            }
            System.out.print(names[50 - 1]);
        }

    public void sort_age_asc(List studentList){
        int temp;
        int age[] = new int[];
        for (int i = 0; i < 50; i++) {
            for (int j = i + 1; j < 50; j++) {
                if (age[j] > age[j+1]) {
                    temp = age[j];
                    age[j] = age[j+1];
                    age[j+1] = temp;
                }
            }
        }
        System.out.print("Names in Sorted Order:");
        for (int i = 0; i < 50 - 1; i++){
            System.out.print(age[i] + ",");
        }
    }

}