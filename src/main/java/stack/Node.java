package stack;

public class Node {
   public Node next;
   public String data;

   public Node(String data){
      this.data = data;
   }

   public void setNext(Node next) {
      this.next = next;
   }

   public String onString(){
      return this.data;
   }

}
