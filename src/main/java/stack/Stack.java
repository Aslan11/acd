package stack;

public class Stack {
    public static class Node{
        private int data;
        private Node next;
        private Node(int data){
            this.data=data;
        }
    }
    private Node head;

    public int peek(){
        return head.data;
    }

    private boolean isEmpty(){
        return head==null;
    }

    public void push(int data){
        Node node=new Node(data);
        node.next=head;
        head=node;
    }

    public int pop(){
        int data=head.data;
        head=head.next;
        return data;
    }
}
