package hashmethod;


public class HashTable {
    private Node table[];
    private int size;
    private Node New;


    public HashTable(int size) {
        table = new Node[size];
        this.size = size;
    }

    public void set(String key, String value) {
    //ADD IMPLEMENTATION HERE
    table[key.hashCode()%size]= new Node(key,value);


    }

    public String get(String key) {

        return table[key.hashCode()%size].getValue();

    }

    public void remove(String key) {
    //WRITE CODE HERE
        table[key.hashCode()%size]=null;

    }

    public int getSize() {
        return size;
    }

    public void printAll() {
    //WRITE CODE HERE
        for(int i=0;i<table.length;i++){
            if (table[i] != null) {
                System.out.println(table[i]);//asl
            }
        }
    }
}
