package quizweek3;

public class LinkedListTest {
    public static void main(String args[]) {
        //creating LinkedList with 5 elements including head
        LinkedList linkedList = new LinkedList();
        Node head = linkedList.head();
        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));

        //finding middle element of LinkedList in single pass
        Node middle = null; //write your code here - start
        int length = 0;
        Node x = head;
        while(x!=null){
            length=length+1;
            x= x.next();
        }
        middle=head;

        //finish here
        for(int i=0;i<length/2;i++){
            middle=middle.next();
        }

        System.out.println("length of LinkedList: " + length);
        System.out.println("middle element of LinkedList : "                                  + middle);

    }
}
