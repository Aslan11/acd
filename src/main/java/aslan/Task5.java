package aslan;

import java.util.Scanner;

public class Task5 {
    static int fib(int n)
    {
        if (n <= 1) {
            return n;
        }
        return fib(n-1) + fib(n-2);
    }

    public  void run ()
    {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(fib(n));
    }
}
