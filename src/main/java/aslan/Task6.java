package aslan;
import java.util.Scanner;
public class Task6 {
    public void run() {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = 1;
        long count = 0;
        power(a, b, c, count);
    }

    void power(int n, int a, int c, long count) {
        int b;
        if (count < n) {
            c *= a;
            if (count == n - 1) {
                System.out.println(c);
            }

            power(n, a, c, count + 1);

        }
    }
}
