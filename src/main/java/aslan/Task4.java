package aslan;
import java.util.Scanner;
public class Task4 {
    public int fact(int a) {
        if (a == 1 || a == 0)
            return 1;
        else {
            a = fact(a - 1) * a;

            return a;
        }
    }

    public void run () {
        int n;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();

        System.out.println(fact(n));
    }
}
