package aslan;
import java.util.Scanner;
public class Task9 {
    public void run() {
        int a, b;
        Scanner scan = new Scanner(System.in);
        a = scan.nextInt();
        b = scan.nextInt();

        int g = fact(a);
        int e = fact(b);
        int f = fact(a - b);

        System.out.println(bin(g, e, f));
    }

    int fact(int n) {
        if (n == 1 || n == 0)
            return 1;
        else {
            n = fact(n - 1) * n;
            return n;
        }
    }

    int bin(int b, int e, int f) {
        int result = b / (e * f);
        return result;
    }

}
