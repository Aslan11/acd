package aslan;
import java.util.Scanner;
public class Task3 {
    public boolean isPrime(int n, int k) {
        if (n==k) return true;
        else if (n % k == 0) return
                false;

        return isPrime(n, k + 1);


    }


    public void run() {
        int a;
        String x = "Prime";
        String y= "Composite";
        Scanner scanner = new Scanner(System.in);
        a = scanner.nextInt();

        if (isPrime(a, 2)) System.out.println(x);
        else System.out.println(y);
    }
}
