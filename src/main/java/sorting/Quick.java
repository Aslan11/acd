package sorting;

public class Quick {
    public static void quicksort(int[] array) {
        quickSort( array,  0, array.length - 1);
    }

    private static void quickSort(int[] array,  int leftstart, int rightend) {
        if (leftstart >= rightend) {
            return;
        }
        int middle = (leftstart + rightend) / 2;
        int pivot=  array[middle];
        int i = leftstart, j = rightend;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }

            while (array[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }


        if (leftstart < j)
            quickSort(array, leftstart, j);

        if (rightend > i)
            quickSort(array, i, rightend);
    }
    public void printall (int [] arr){
        for (int i = 0; i<arr.length;i++){
            System.out.println(arr [i] + " ");
        }
    }
    public static void main(String[] args) {
        Quick quick = new Quick();
        int arr[] = {8,5,95,45,78,69,23,42};
        quick.quicksort(arr);
        quick.printall(arr);
    }
}