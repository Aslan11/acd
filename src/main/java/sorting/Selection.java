package sorting;



public class Selection {

    private static boolean less(Comparable v,Comparable w){
        boolean b = v.compareTo(w)<0;
        return b;
    }

    private static void exch(Comparable[] a, int i, int j){
        Comparable t=a[i]; a[i]=a[j];a[j]=t;
    }


    public void sort(Comparable[] arr) {
        int n= arr.length;
        for(int i=0;i<n;i++){
            int min=i;
            Comparable[] v;
            for(int j = i+1; j<n; j++)
                if(less(v[j],v[min])) min=j;
                exch(arr,i,min);
        }

    }

}
